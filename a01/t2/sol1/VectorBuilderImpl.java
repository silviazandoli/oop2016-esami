package ex2016.a01.t2.sol1;

import java.util.*;


public class VectorBuilderImpl<X> implements VectorBuilder<X> {

    private final List<X> list = new ArrayList<X>();
    private boolean done = false;
    
    @Override
    public void addElement(X x) {
        this.list.add(x);
    }
    
    @Override
    public void removeElement(int position) {
        this.list.remove(position);   
    }

    @Override
    public void reverse() {
        Collections.reverse(this.list);
    }

    @Override
    public void clear() {
        this.list.clear();
    }
    
    @Override
    public Optional<Vector<X>> build() {
        return this.buildWithFilter(x -> true);
    }
    
    @Override
    public Optional<Vector<X>> buildWithFilter(Filter<X> filter) {
        if (this.done || !this.list.stream().allMatch(filter::check)) {
            return Optional.empty();
        }
        this.done = true;
        return Optional.of(new VectorImpl<X>(this.list));
    }
    
    @Override
    public <Y> VectorBuilder<Y> mapToNewBuilder(Mapper<X, Y> mapper) {
        final VectorBuilder<Y> builder = new VectorBuilderImpl<>();
        this.list.forEach(x -> builder.addElement(mapper.transform(x)));
        return builder;
    }

}
